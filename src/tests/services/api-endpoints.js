import test from "ava";
import { MatrixApi } from "../../services/api.js";
import { before } from "../../utils/tests.js";

test.before(async (t) => {
	/* insert the logic we need in every test */
	before(t);
	const baseUrl = t.context.base_url;

	const apiGuest = new MatrixApi({ baseUrl, storageKey: "mre-test-guest" });
	const apiUser = new MatrixApi({ baseUrl, storageKey: "mre-test-user" });

	/* assign an api instance with a non-default host */
	t.context.apiGuest = apiGuest;
	t.context.apiUser = apiUser;
});

/*
	 Test the endpoints fetching methods of the MatrixApi
 */
test.serial("Guest user can register", async (t) => {
	const api = new MatrixApi({
		baseUrl: t.context.base_url,
		storageKey: "mretest-register-guest-user",
	});
	const res = await api.registerGuestUser();
	t.truthy(api.guestUser);
});

test.serial("Guest user is registered by calling fetch()", async (t) => {
	const api = t.context.apiGuest;
	const res = await api.getUserProfile("@matthew:matrix.org");
	t.truthy(api.guestUser);
});

test.serial("Guest can join a room with allow guest access", async (t) => {
	const api = t.context.apiGuest;
	const resJoin = await api.joinRoom(t.context.room_guest_id);
	t.true(resJoin.errcode === "M_CONSENT_NOT_GIVEN");
	/* {
		 consent_uri: 'https://matrix-client.matrix.org/_matrix/consent?u=56552315989&h=c5002ec50e741edc6eb0e6f6416e3356dfbc4d876e4449c1feba4631d2fc513b',
		 errcode: 'M_CONSENT_NOT_GIVEN',
		 error: 'To continue using the matrix.org homeserver you must review and agree to our terms and conditions at https://matrix-client.matrix.org/_matrix/consent?u=1234abcd.'
		 } */
});

test.serial(
	"access_token is not added in headers by buildFetchConfig",
	async (t) => {
		const api = t.context.apiGuest;
		const res = await api.fetch();
		const config = api.buildFetchConfig();
		t.is(config.headers["Authorization"], undefined);
	}
);
test.serial(
	"access_token is added in headers by buildFetchConfig when specified",
	async (t) => {
		const api = t.context.apiGuest;
		const res = await api.fetch();
		const config = api.buildFetchConfig({}, true);
		t.true(config.headers["Authorization"].startsWith("Bearer"));
	}
);

test.serial("getRoomId converts a room alias to a room id", async (t) => {
	const api = t.context.apiGuest;
	const roomProfile = api.checkMatrixId(t.context.room_alias);
	const spaceProfile = api.checkMatrixId(t.context.space_alias);
	const { room_id: roomId } = await api.getRoomId({
		roomAlias: roomProfile.roomAlias,
		host: roomProfile.host,
	});
	const { room_id: spaceId } = await api.getRoomId({
		roomAlias: spaceProfile.roomAlias,
		host: spaceProfile.host,
	});
	t.true(roomId === t.context.room_id);
	t.true(spaceId === t.context.space_id);
});

test.serial(
	"User can login with @username:domain.tld and paswword",
	async (t) => {
		const { apiUser: api, user_id, password } = t.context;
		const resLogin = await api.login({ user_id, password });
		t.is(resLogin.user_id, user_id);
		t.truthy(api.user);
		t.true(api.auth);
	}
);

test.serial("User can create a room", async (t) => {
	const { apiUser: api, user_id, password } = t.context;
	const resLogin = await api.login({ user_id, password });
	const resCreate = await api.createRoom({
		name: "test",
		topic: "tests",
	});
	t.truthy(resCreate.room_id);
	/* { room_id: '!RZolXuEqgKSdXNQrLE:matrix.org' } */
});

test.serial("User can list joined room", async (t) => {
	const { apiUser: api, user_id, password } = t.context;
	const resLogin = await api.login({ user_id, password });
	const { joined_rooms } = await api.getJoinedRooms();
	t.is(typeof joined_rooms, "object");
});

test.serial("User can leave a room", async (t) => {
	const { apiUser: api, user_id, password } = t.context;
	const resLogin = await api.login({ user_id, password });
	const { room_id } = await api.createRoom({
		name: "test_leave",
		topic: "test_leave_room",
	});
	const resLeave = await api.leaveRoom(room_id);
	t.deepEqual(resLeave, {});
	// {}
});

test.serial("User can forget a room", async (t) => {
	const { apiUser: api, user_id, password } = t.context;
	const resLogin = await api.login({ user_id, password });
	const resCreate = await api.createRoom({
		name: "test_forget",
		topic: "test_forget_room",
	});
	const resLeave = await api.leaveRoom(resCreate.room_id);
	const resForget = await api.forgetRoom(resCreate.room_id);
	t.deepEqual(resForget, {});
});

test.serial("User can search a user by their user ID", async (t) => {
	const api = t.context.apiUser;
	await api.login({
		user_id: t.context.user_id,
		password: t.context.password,
	});
	const userSearch = await api.searchUsers(t.context.user_id, 2);
	const loggedInUser = userSearch.results.find(user => user.user_id === t.context.user_id)
	t.true(loggedInUser.user_id === t.context.user_id);
});

test.serial("Guest user can find a user profile by user ID", async (t) => {
	const api = t.context.apiGuest;
	const userProfile = await api.getUserProfile(t.context.user_id);
	t.truthy(userProfile.displayname);
});

test.serial("Guest user can edit their profile", async (t) => {
	const api = t.context.apiGuest;
	const displayname = `mwc_test_${Date.now()}`;
	const avatar_url = "https://example.org/my-image.gif";
	await api.setUserProfile("displayname", displayname);
	/* await api.setUserProfile("avatar_url", avatar_url); */
	const userProfile = await api.getUserProfile(api.guestUser.user_id);
	t.true(userProfile.displayname === displayname);
	/* t.true(userProfile.avatar_url === avatar_url); */
	/* t.true(userProfile.my_key === displayname); */
});

test.skip("User can set their presence status", async (t) => {
	const api = t.context.apiUser;
	const user_id = t.context.user_id;
	await api.login({
		user_id,
		password: t.context.password,
	});
	const status_msg = `mwc_test_status_${Date.now()}`;
	const presence = "unavailable";
	const res = await api.setUserStatus({ presence, status_msg });
	const statusRes = await api.getUserStatus(user_id);
	const { presence: presenceRes, status_msg: status_msgRes } = statusRes;
	// somehow it does not update..
	t.true(presence === presenceRes);
	t.true(status_msg === status_msgRes);
});
