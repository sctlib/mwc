import "dotenv/config";
import test from "ava";
import { before } from "../../utils/tests.js";
import matrixApi, {
	MatrixApi,
	DEFAULT_BASE_URL,
	DEFAULT_BASE_API,
} from "../../services/api.js";

test.before((t) => before(t));

/*
	 Test the static methods of the MatrixApi
 */

test.serial("Exported MatrixApi is a EventTarget", (t) => {
	t.pass(MatrixApi instanceof EventTarget);
});

test.serial("Exported api singleton is instance of MatrixApi", (t) => {
	t.true(matrixApi instanceof MatrixApi);
});

test.serial(`new MatrixApi has correct initial state`, (t) => {
	const api = new MatrixApi();
	t.true(api.baseUrl === undefined);
	t.true(api.baseApi === DEFAULT_BASE_API);
});

test.serial(`new MatrixApi has correct initial state when with config`, (t) => {
	const baseUrl = "https://example.org";
	const api = new MatrixApi({ baseUrl });
	t.true(api.baseUrl === baseUrl + "/");
});

test.serial("new MatrixApi has no logged in (guest)user(s)", (t) => {
	const api = new MatrixApi();
	t.true(!api.user);
	t.true(!api.guestUser);
	t.true(!api.auth);
	t.true(!api.authGuest);
	t.true(!api.authAny);
});

test.serial("checkMatrixId finds a user by id", (t) => {
	const profile = matrixApi.checkMatrixId("@hello:world.net");
	t.true(profile.user === "hello");
	t.true(profile.host === "world.net");
});

test.serial("checkMatrixId finds a room by id", (t) => {
	const profile = matrixApi.checkMatrixId("!123abc:example.com");
	t.true(profile.roomId === "123abc");
	t.true(profile.host === "example.com");
});

test.serial("checkMatrixId finds a room by alias", (t) => {
	const profile = matrixApi.checkMatrixId("#mre:matrix.org");
	t.true(profile.roomAlias === "mre");
	t.true(profile.host === "matrix.org");
});

test.serial("buildFetchUrl adds search params to a url", (t) => {
	const url = matrixApi.buildFetchUrl({
		baseUrl: "https://example.org",
		baseApi: "_test",
		version: "0",
		endpoint: `this/that`,
		params: [
			["foo", true],
			["bar", false],
		],
	});
	t.true(url.searchParams.get("foo") === "true");
	t.true(url.searchParams.get("bar") === "false");
});
