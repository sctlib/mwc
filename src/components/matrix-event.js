import { toTimestamp } from "../utils/dates.js";
import matrixApi from "../services/api.js";
import eventsManager from "../services/events-manager.js";
import MX_DOM_EVENTS from "../services/dom-events.js";
const { ON_EVENT_SELECT, ON_EVENT_ACTION } = MX_DOM_EVENTS;

export default class MatrixEvent extends HTMLElement {
	static get observedAttributes() {
		return [
			"event",
			"event-id",
			"profile-id",
			"event-type",
			"origin",
			"show-info",
			"show-sender",
			"show-actions",
		];
	}
	get origin() {
		return this.getAttribute("origin") || ``;
	}
	get profileId() {
		return this.getAttribute("profile-id");
	}
	get event() {
		return JSON.parse(this.getAttribute("event"));
	}
	set event(obj) {
		this.setAttribute("event", JSON.stringify(obj));
	}
	get eventId() {
		return this.getAttribute("event-id");
	}
	set eventId(str) {
		return this.setAttribute("event-id", str);
	}
	get eventType() {
		return this.getAttribute("event-type");
	}
	set eventType(str) {
		return this.setAttribute("event-type", str);
	}
	get messageType() {
		return this.getAttribute("message-type");
	}
	set messageType(str) {
		return this.setAttribute("message-type", str);
	}
	get eventHref() {
		return `${this.origin}/${this.profileId}/${this.event.event_id}`;
	}
	get showInfo() {
		return this.getAttribute("show-info") === "true";
	}
	get showSender() {
		return this.getAttribute("show-sender") === "true";
	}
	get showActions() {
		return this.getAttribute("show-actions") === "true";
	}
	get flagSanitizer() {
		return this.getAttribute("flag-sanitizer") === "true";
	}
	get authIsSender() {
		return matrixApi.isUserEventSender(this.event);
	}

	get eventTypeExsists() {
		return (
			this.event?.type &&
			eventsManager.eventTypes.get(this.event.type)?.displayTemplate
		);
	}

	onClick(event) {
		/* we pass the 'click' event, to be able to preventDefault */
		const eventSelect = new CustomEvent(ON_EVENT_SELECT, {
			bubbles: true,
			detail: {
				event: event,
				mxevent: this.event,
			},
		});
		this.dispatchEvent(eventSelect);
	}
	async onAction(event) {
		const {
			target: { value },
		} = event.detail;
		if (value === "redact") {
			const confirmRedact = prompt(
				"Really redact this event? You can optionally explain why."
			);
			if (confirmRedact !== null) {
				try {
					await matrixApi.redactEvent({
						room_id: this.event.room_id,
						event_id: this.event.event_id,
						reason: confirmRedact,
					});
					this.onRedactEvent();
				} catch (error) {
					console.log("Error redacting event", error);
				}
			}
		}
		if (value === "edit") {
			console.log("event action:", value, event.detail);
		}
	}

	async connectedCallback() {
		this.$root = this;
		this.$root.addEventListener("click", this.onClick.bind(this));

		if (!this.event && this.eventId && this.profileId) {
			const apiEvent = await matrixApi.getProfileEvent({
				eventId: this.eventId,
				profileId: this.profileId,
			});
			this.event = apiEvent;
		}

		if (!this.event) {
			return;
		}

		const { event_id, content, type: event_type } = this.event;

		this.eventId = event_id;
		this.eventType = event_type;

		/* also set the message type as attribute, if there is */
		if (content.msgtype) {
			this.messageType = content.msgtype;
		}

		this.render();
	}
	disconnectedCallback() {
		this.$root && this.$root.removeEventListener("click", this.onClick);
	}
	render() {
		if (this.showInfo) {
			const $eventState = this.buildEventState();
			this.append($eventState);
		}

		const $eventContent = this.buildEventContent();
		this.append($eventContent);
	}

	/* render the event content depending on its type */
	buildEventType() {
		const $template = eventsManager.getDisplayTemplate(this.event.type);
		if ($template) {
			const $eventTemplate = $template.content.cloneNode(true);
			const $firstChildren = $eventTemplate.children[0];
			if ($firstChildren) {
				$firstChildren.setAttribute("event", JSON.stringify(this.event));
				this.flagSanitizer &&
					$firstChildren.setAttribute("flag-sanitizer", this.flagSanitizer);
			}
			return $eventTemplate;
		}
	}
	buildEventContent() {
		const $eventContent = document.createElement("matrix-event-content");
		if (this.eventTypeExsists) {
			const $eventType = this.buildEventType();
			$eventContent.append($eventType);
		} else {
			$eventContent.append(
				`No registered displayTemplate for event type ${this.event.type}`
			);
		}
		return $eventContent;
	}

	buildEventState() {
		const $timestamp = document.createElement("time");
		const timestamp = toTimestamp(this.event.origin_server_ts);
		$timestamp.setAttribute("datetime", timestamp);

		const $sender = document.createElement("b");
		$sender.setAttribute("title", this.event.sender);
		$sender.innerText = this.event.sender;

		if (this.origin) {
			const $link = document.createElement("a");
			$link.href = this.eventHref;
			$link.innerText = timestamp;
			$timestamp.append($link);
		} else {
			$timestamp.innerText = timestamp;
		}

		const $info = document.createElement("matrix-event-state");
		$info.append($timestamp);

		if (this.showSender) {
			$info.append($sender);
		}

		/* render the actions for this event  */
		if (this.showActions) {
			if (this.origin || this.authIsSender) {
				const $eventAction = this.buildEventActions();
				$info.append($eventAction);
			}
		}

		return $info;
	}
	buildEventActions() {
		const $actions = document.createElement("matrix-event-actions");
		$actions.setAttribute("sender", this.authIsSender);
		$actions.setAttribute("profile-id", this.profileId);
		$actions.setAttribute("event-id", this.eventId);
		$actions.setAttribute("origin", this.origin);
		$actions.setAttribute("default-text", "...");
		$actions.addEventListener(ON_EVENT_ACTION, this.onAction.bind(this));
		return $actions;
	}

	onRedactEvent() {
		this.replaceChildren("Event redacted.");
	}
}
