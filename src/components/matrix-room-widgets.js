export default class MatrixEvent extends HTMLElement {
	static get observedAttributes() {
		return ["widgets"];
	}
	get widgets() {
		return JSON.parse(this.getAttribute("widgets")) || [];
	}
	set widgets(obj) {
		this.setAttribute("widgets", JSON.stringify(obj));
	}
	constructor() {
		super();
	}
	attributeChangedCallback() {
		this.render();
	}
	connectedCallback() {
		this.render();
	}
	render() {
		this.innerHTML = "";
		const $widgets = this.widgets
			.map((widget) => {
				const $iframe = document.createElement("iframe");
				const { content, data } = widget.content;
				const { type } = content || {};
				if (type === "youtube") {
					$iframe.src = data.cUrl;
				} else if (type === "etherpad") {
				} else if (data.url) {
					$iframe.src = data.url;
				}
				if ($iframe.src) {
					const $widget = document.createElement("matrix-room-widget");
					$widget.append($iframe);
					return $widget;
				}
			})
			.filter(($widget) => !!$widget);
		this.append(...$widgets);
	}
}
