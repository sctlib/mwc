import matrixApi from "../services/api.js";

export default class MatrixUser extends HTMLElement {
	get user() {
		return matrixApi.user;
	}
	get guestUser() {
		return matrixApi.guestUser;
	}
	get showGuest() {
		return this.getAttribute("show-guest") === "true";
	}
	handleAuth(event) {
		console.log("auth", event);
		this.render();
	}
	constructor() {
		super();
		matrixApi.addEventListener("auth", this.handleAuth.bind(this));
	}
	connectedCallback() {
		this.render();
	}
	disconnectedCallback() {
		matrixApi.removeEventListener("auth", this.handleAuth.bind(this));
	}
	render() {
		this.replaceChildren();
		if (this.showGuest) {
			if (this.guestUser) {
				this.appendDomsAsList(this.buildGuestUser());
			} else {
				this.appendDomsAsList(this.buildNoUserGuest());
			}
		} else {
			if (this.user) {
				const $userDoms = this.buildUser();
				this.appendDomsAsList($userDoms);
			} else {
				this.appendDomsAsList(this.buildNoUser());
			}
		}
	}
	appendDomsAsList($doms) {
		const $list = document.createElement("ul");
		$list.append(...$doms);
		this.append($list);
	}
	buildListItem(domItem, index) {
		const $listItem = document.createElement("li");
		$listItem.append(domItem);
		return $listItem;
	}
	buildNoUser(isGuest = false) {
		const text = isGuest
			? "No guest matrix user logged in"
			: "No registered matrix user logged in";
		const $text = document.createElement("span");
		$text.textContent = text;
		return [$text].map(this.buildListItem);
	}
	buildNoUserGuest() {
		return this.buildNoUser(true);
	}
	buildGuestUser() {
		return [
			this.buildUserId(this.guestUser.user_id),
			this.buildHomeserver(this.guestUser.home_server),
			this.buildDeviceId(this.guestUser.device_id),
		].map(this.buildListItem);
	}
	buildUser() {
		return [
			this.buildUserId(this.user.user_id),
			this.buildHomeserver(this.user.home_server),
			this.buildDeviceId(this.user.device_id),
			this.buildWellKnown(this.user.well_known),
		]
			.filter((dom) => !!dom)
			.map(this.buildListItem);
	}
	buildUserId(userId) {
		const $dom = document.createElement("matrix-user-id");
		$dom.textContent = userId;
		return $dom;
	}
	buildDeviceId(deviceId) {
		const $dom = document.createElement("matrix-user-device-id");
		$dom.textContent = deviceId;
		return $dom;
	}
	buildHomeserver(homeserver) {
		const $dom = document.createElement("matrix-user-homeserver");
		$dom.textContent = homeserver;
		return $dom;
	}
	buildWellKnown(wk) {
		if (wk) {
			const $dom = document.createElement("matrix-user-well-known");
			$dom.textContent = JSON.stringify(wk, undefined, 2);
			return $dom;
		}
	}
	buildAccessToken() {
		const $dom = document.createElement("matrix-user-access-token");
		$dom.textContent = "*****";
		return $dom;
	}
}
