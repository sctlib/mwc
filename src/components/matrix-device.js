import { toTimestamp } from "../utils/dates.js";
import matrixApi from "../services/api.js";

/* an abstract component (custom element), connected to the matrix api */
export default class MatrixDevice extends HTMLElement {
	/* props */
	get deviceId() {
		return this.getAttribute("device-id");
	}
	set device(obj) {
		return this.setAttribute("device", JSON.stringify(obj));
	}
	get device() {
		return JSON.parse(this.getAttribute("device"));
	}
	get isCurrent() {
		return this.api.isCurrentDeviceId(this.device.device_id);
	}

	/* lifecycle */
	constructor() {
		super();
		this.api = matrixApi;
	}
	async connectedCallback() {
		if (!this.device && this.deviceId) {
			try {
				const res = await matrixApi.getDevice(this.deviceId);
				const { device_id, error } = res;
				if (!device_id && error) {
					throw res;
				} else {
					this.device = res;
				}
			} catch (error) {
				this.error = error;
			}
		}
		this.render();
	}
	render() {
		const $doms = [];
		if (this.device) {
			$doms.push(this.createDevice(this.device));
		}
		if (this.error) {
			$doms.push(this.createError(this.error));
		}
		this.replaceChildren(...$doms);
	}
	createDevice({
		device_id,
		display_name,
		last_seen_ip,
		last_seen_ts,
		user_id,
	} = {}) {
		const $displayName = document.createElement("matrix-device-display-name");
		if (display_name) {
			$displayName.textContent = display_name;
		} else {
			const $displayNameForm = document.createElement("form");
		}

		const $lastSeenIp = document.createElement("matrix-device-last-seen-ip");
		$lastSeenIp.textContent = last_seen_ip;

		const timestamp = toTimestamp(last_seen_ts);
		const $lastSeenTs = document.createElement("matrix-device-last-seen-ts");
		const $timestamp = document.createElement("time");
		$timestamp.setAttribute("datetime", last_seen_ts);
		$timestamp.textContent = timestamp;
		$lastSeenTs.append($timestamp);

		const $userId = document.createElement("matrix-device-user-id");
		$userId.textContent = user_id;

		const $deviceId = document.createElement("matrix-device-id");
		const $deviceIdCode = document.createElement("code");
		$deviceIdCode.textContent = device_id;
		$deviceId.append($deviceIdCode);

		const $term = document.createElement("dt");
		$term.append($deviceId, $displayName);

		const $deviceDefinitions = [];
		if (this.isCurrent) {
			$deviceDefinitions.push("(current device)");
		}
		$deviceDefinitions.push($userId, $lastSeenTs, $lastSeenIp);
		const $defs = $deviceDefinitions.map(($d) => {
			const $def = document.createElement("dd");
			$def.append($d);
			return $def;
		});
		const $definitionList = document.createElement("dl");
		$definitionList.append($term, ...$defs);
		return $definitionList;
	}
	createError(error) {
		const $error = document.createElement("matrix-error");
		$error.setAttribute("error", JSON.stringify(error));
		return $error;
	}
}
