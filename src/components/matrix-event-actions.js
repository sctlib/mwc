import MatrixApi from "../services/api.js";
import MX_DOM_EVENTS from "../services/dom-events.js";
const { ON_EVENT_ACTION } = MX_DOM_EVENTS;

/*
	 Actions regarding an event;
	 differs if user is (un)authed, sender (owner) of the event etc.
	 Not supposed to be used independently, outside of <matrix-event/>
 */
export default class MatrixEventActions extends HTMLElement {
	static get observedAttributes() {
		return ["sender", "event-id", "profile-id", "origin"];
	}
	get origin() {
		return this.getAttribute("origin");
	}
	get profileId() {
		return this.getAttribute("profile-id");
	}
	get eventId() {
		return this.getAttribute("event-id");
	}
	get defaultText() {
		return this.getAttribute("default-text") || "Menu";
	}
	get sender() {
		return this.getAttribute("sender") === "true";
	}
	/* helpers */
	get eventHref() {
		if (this.origin && this.profileId && this.eventId) {
			return `${this.origin}/${this.profileId}/${this.eventId}`;
		}
	}
	/* events */
	_onInputShareClick({ target }) {
		target.focus();
		target.select();
	}

	async connectedCallback() {
		this.$form = document.createElement("form");

		this.$select = document.createElement("select");
		this.$select.addEventListener("input", this.onInput.bind(this));

		const $dialog = document.createElement("wcu-dialog");

		const $dialogSlot = document.createElement("aside");
		$dialogSlot.setAttribute("slot", "dialog");
		const $openSlot = document.createElement("span");
		$openSlot.setAttribute("slot", "open");

		$dialog.append($dialogSlot, $openSlot);
		this.$dialog = $dialog;
		this.$dialogContent = $dialogSlot;

		this.$form.append(this.$select);
		this.$form.append($dialog);
		this.append(this.$form);
		this.render();
	}
	disconnectedCallback() {
		this.$root && this.$root.removeEventListener("change", this.onInput);
	}
	render() {
		const $default = document.createElement("option");
		$default.innerText = this.defaultText;
		$default.value = "default";
		$default.setAttribute("disabled", true);
		$default.setAttribute("selected", true);

		const $share = document.createElement("option");
		$share.innerText = "Share";
		$share.value = "share";

		const $redact = document.createElement("option");
		$redact.innerText = "Redact";
		$redact.value = "redact";

		const $edit = document.createElement("option");
		$edit.innerText = "Edit";
		$edit.value = "edit";

		this.$select.append($default);
		this.eventHref && this.$select.append($share);
		this.sender && this.$select.append($redact);
		false && this.sender && this.$select.append($edit);
	}

	onInput(event) {
		const {
			target: { value },
		} = event;

		if (value === "share") {
			this.renderDialogShare();
		}
		const actionEvent = new CustomEvent(ON_EVENT_ACTION, {
			bubbles: true,
			detail: event,
		});
		this.dispatchEvent(actionEvent);
		this.resetValue();
	}
	resetValue() {
		this.$select.selectedIndex = 0;
	}
	renderDialogShare() {
		this.$dialog.open();
		const text = "Share a link to this event";
		const url = this.eventHref;
		const $copyLabel = document.createElement("label");
		$copyLabel.innerText = text;
		const $copyInput = document.createElement("input");
		$copyInput.value = url;
		$copyInput.readonly = true;
		$copyInput.addEventListener("click", this._onInputShareClick.bind(this));
		$copyLabel.append($copyInput);
		this.$dialogContent.replaceChildren($copyLabel);
	}
}
