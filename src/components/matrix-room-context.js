const template = `<slot name="events"></slot>`;

export default class MatrixRoomContext extends HTMLElement {
	static get observedAttributes() {
		return [
			"origin",
			"filter",
			"show-info",
			"show-sender",
			"show-actions",
			"show-context",
			"events",
			"profile-id",
		];
	}
	get origin() {
		return this.getAttribute("origin") || ``;
	}
	get flagSanitizer() {
		return this.getAttribute("flag-sanitizer") === "true";
	}
	get showInfo() {
		return this.getAttribute("show-info") === "true";
	}
	get showSender() {
		return this.getAttribute("show-sender") === "true";
	}
	get showActions() {
		return this.getAttribute("show-actions") === "true";
	}
	get showContext() {
		return this.getAttribute("show-context") === "true";
	}
	get filter() {
		return JSON.parse(this.getAttribute("filter"));
	}
	get events() {
		return JSON.parse(this.getAttribute("events")) || [];
	}
	get profileId() {
		return this.getAttribute("profile-id");
	}
	get dir() {
		const direction = this.getAttribute("dir");
		if (direction === "after") return "after";
		if (direction === "before") return "before";
		return "initial";
	}
	/* only the initial context has both dirs */
	get loadBefore() {
		if (this.dir === "before" || this.dir === "initial") return true;
		return false;
	}
	get loadAfter() {
		if (this.dir === "after" || this.dir === "initial") return true;
		return false;
	}

	attributeChangedCallback() {
		this.init();
	}
	connectedCallback() {
		this.init();
	}
	init() {
		this.innerHTML = template;
		this.$events = this.querySelector('slot[name="events"]');
		this.renderEvents({
			events: this.events,
		});
		this.showContext && this.renderButtons();
	}
	renderButtons() {
		if (this.loadBefore) {
			/* prepend button before */
			const $loadBefore = document.createElement("button");
			$loadBefore.setAttribute("dir", "before");
			$loadBefore.addEventListener("click", this.loadMoreContext.bind(this));
			$loadBefore.innerText = "load more before";
			this.$events.append($loadBefore);
		}
		if (this.loadAfter) {
			/* append button after */
			const $loadAfter = document.createElement("button");
			$loadAfter.setAttribute("dir", "after");
			$loadAfter.addEventListener("click", this.loadMoreContext.bind(this));
			$loadAfter.innerText = "load more after";
			this.$events.prepend($loadAfter);
		}
	}
	filterRedactedEvent(event = {}) {
		if (!event) return false;
		const { type, redacted_because, content = {}, unsigned = {} } = event;
		const { "m.new_content": newContent } = content;

		/* somehow this data is sometimes in the "unsigned" object,
			 even when redacted by logged in user (in mre) */
		const { redacted_because: unsigned_redacted_because } = unsigned;

		/* dont't show the event if has the key redacted_because,
			 which means it has been redacted (by user or admin etc.)*/
		if (redacted_because || unsigned_redacted_because) {
			return false;
		} else if (newContent) {
			return false;
		} else if (!content) {
			return false;
		}
		return true;
	}
	renderEvents({ events = [] }) {
		if (!events.length) return;
		events.filter(this.filterRedactedEvent).forEach((event) => {
			const $event = document.createElement("matrix-event");
			$event.setAttribute("event", JSON.stringify(event));
			$event.setAttribute("profile-id", this.profileId);
			if (this.origin) {
				$event.setAttribute("origin", this.origin);
			}
			if (this.showInfo) {
				$event.setAttribute("show-info", true);
			}
			if (this.showSender) {
				$event.setAttribute("show-sender", true);
			}
			if (this.showActions) {
				$event.setAttribute("show-actions", true);
			}
			if (this.flagSanitizer) {
				$event.setAttribute("flag-sanitizer", true);
			}
			/* append event */
			this.$events.append($event);
		});
	}
	loadMoreContext(event) {
		const $target = event.target; // ready to remove from dom, because clicked
		/* 1. render locally available events */
		/* 2. send event above to load next batch;
			 `event` is the event from which to get context */
		const dir = event.target.getAttribute("dir");

		const events = this.events;

		let newContextEvent;
		if (dir === "before") {
			newContextEvent = events[events.length - 1];
		} else if (dir === "after") {
			newContextEvent = events[0];
		}
		const eventContext = new CustomEvent("loadcontext", {
			bubbles: true,
			detail: {
				event: newContextEvent,
				dir,
			},
		});
		this.dispatchEvent(eventContext);
		$target.remove();
	}
}
