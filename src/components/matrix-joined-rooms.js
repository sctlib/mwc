import MatrixApi from "../services/api.js";

export default class MatrixJoinedRooms extends HTMLElement {
	static get observedAttributes() {
		return ["rooms", "origin"];
	}

	get origin() {
		return this.getAttribute("origin");
	}

	get rooms() {
		return JSON.parse(this.getAttribute("rooms"));
	}
	set rooms(arr = []) {
		this.setAttribute("rooms", JSON.stringify(arr));
	}
	constructor() {
		super();
		MatrixApi.addEventListener("auth", this.onAuth.bind(this));
	}
	async connectedCallback() {
		await this.init();
		this.render();
	}
	async init() {
		const { joined_rooms } = await MatrixApi.getJoinedRooms();
		this.rooms = joined_rooms;
	}
	async onAuth() {
		await this.init();
		this.render();
	}
	render() {
		this.innerHTML = "";
		if (this.rooms && this.rooms.length) {
			this.renderRooms();
		} else {
			this.renderNoRooms();
		}
	}
	renderNoRooms() {
		const $noRoom = document.createElement("span");
		$noRoom.innerText = "You haven't joined any room yet.";
		this.append($noRoom);
	}
	renderRooms() {
		const $roomsMenu = document.createElement("menu");
		this.rooms.forEach((room) => {
			const $room = document.createElement("li");
			const $mre = document.createElement("matrix-room");
			$mre.setAttribute("profile-id", room);
			this.origin && $mre.setAttribute("origin", this.origin);
			$room.append($mre);
			$roomsMenu.append($room);
		});
		this.append($roomsMenu);
	}
}
