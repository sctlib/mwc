import { MatrixApiWidget } from "../services/api-widget.js";

const UI_CAPABILITIES_ACTIONS = {
	set_always_on_screen: {
		text: "➤",
		title: "Toggle always on screen",
		normalizeData: (inputEvent) => {
			const { checked } = inputEvent.target;
			return {
				value: checked,
			};
		},
	},
};

export default class MatrixWidget extends HTMLElement {
	static get observedAttributes() {
		return ["capabilites", "capabilites-search-param"];
	}
	/* props */
	get capabilities() {
		let raw;
		if (this.capabilitiesSearchParam) {
			const params = new URLSearchParams(window.location.search);
			raw = params.get(this.capabilitiesSearchParam);
		} else {
			raw = this.getAttribute("capabilities");
		}
		const caps = raw?.split(",").filter((val) => !!val);
		return caps || [];
	}
	get capabilitiesSearchParam() {
		return this.getAttribute("capabilites-search-param") || "capabilites";
	}
	get canReceiveEvents() {
		return (
			this.api.receiveEventTypes?.length ||
			this.api.capabilitiesApproved?.filter((cap) => {
				return cap.startsWith("org.matrix.msc2762.timeline:");
			}).length
		);
	}
	get canSendEvents() {
		return !!this.api.sendEventTypes?.length;
	}

	/* dom helpers */
	get $ui() {
		return this.querySelector("matrix-widget-ui");
	}
	get $info() {
		return this.querySelector("matrix-widget-info");
	}
	get $send() {
		return this.querySelector("matrix-widget-send");
	}
	get $receive() {
		return this.querySelector("matrix-widget-receive");
	}

	/* state */
	actionsState = new Map();
	receivedEvents = [];

	/* events */
	onNotifiedCapabilities({ detail = {} }) {
		this.render();
	}
	onClientSendEvent({ detail = {} }) {
		const { data } = detail;
		this.receivedEvents = [data, ...this.receivedEvents];
		this.renderReceive();
	}
	async onMxSend(event) {
		let res;
		try {
			res = await this.api.sendEvent(event.detail);
			console.log("Send event res", res);
		} catch (error) {
			console.log("Error sending event", error);
		}
	}
	async onCapabilityUIAction(event) {
		const { name: action } = event.target;
		const capability = this.api.getActionCapability(action);
		const actionState = this.actionsState.get(action);
		if (this.api.hasCapability(capability)) {
			const normalizeData = UI_CAPABILITIES_ACTIONS[action].normalizeData;
			const data = normalizeData(event);

			try {
				const res = await this.api.sendClientAction(action, data);
			} catch (error) {
				console.log("widget sendClientAction error", error);
			}

			this.actionsState.set(action, data);
		}
		this.renderUI();
	}

	/* lifecycle */
	connectedCallback() {
		this.api = new MatrixApiWidget({
			capabilities: this.capabilities,
		});
		this.api.addEventListener(
			`${this.api.DOM_EVENT_ROOT}.notify_capabilities`,
			this.onNotifiedCapabilities.bind(this)
		);
		this.api.addEventListener(
			`${this.api.DOM_EVENT_ROOT}.send_event`,
			this.onClientSendEvent.bind(this)
		);
		this.api.start();
		this.render();
	}
	disconnectedCalback() {
		this.api.removeEventListener(
			`${this.api.DOM_EVENT_ROOT}.notify_capabilities`,
			this.onNotifiedCapabilities
		);
		if (this.api.capabilitiesApproved?.indexOf("m.always_on_screen") > -1) {
			/* added by the notified capabilities */
			this.api.setAlwaysOnScreen(false);
		}
		this.api.removeEventListener(
			`${this.api.DOM_EVENT_ROOT}.send_event`,
			this.onClientSendEvent
		);
	}
	render() {
		this.renderUI();
		this.renderInfo();
		this.renderSend();
		this.renderReceive();
	}
	renderUI() {
		const $ui = this.createUi(this.api.capabilitiesActions);
		if (this.$ui) {
			this.$ui.replaceWith($ui);
		} else {
			this.append($ui);
		}
	}
	renderInfo() {
		const $info = this.createInfo({
			urlConfig: this.api.urlConfig,
			capabilities: this.capabilities,
			capabilitiesApproved: this.api.capabilitiesApproved,
		});
		if (this.$info) {
			this.$info.replaceWith($info);
		} else {
			this.append($info);
		}
	}
	renderSend() {
		if (this.canSendEvents) {
			const $sendCapabilities = this.createSendCapabilities({
				sendEventTypes: this.api.sendEventTypes,
			});
			if (this.$send) {
				this.$send.replaceWith($sendCapabilities);
			} else {
				this.append($sendCapabilities);
			}
		} else {
			this.$send?.remove();
		}
	}
	renderReceive() {
		if (this.canReceiveEvents) {
			const $receiveCapabilities = this.createReceiveCapabilities({
				receivedEvents: this.receivedEvents,
			});
			if (this.$receive) {
				this.$receive.replaceWith($receiveCapabilities);
			} else {
				this.append($receiveCapabilities);
			}
		} else {
			this.$receive?.remove();
		}
	}
	createInfo({ urlConfig = {}, capabilities = [], capabilitiesApproved }) {
		const { widgetId } = urlConfig;

		const $id = document.createElement("matrix-widget-id");
		if (widgetId) {
			const $idPre = document.createElement("code");
			$idPre.textContent = widgetId || "No id (not a widget)";
			$id.append($idPre);
		} else {
			const $idLink = document.createElement("a");
			$idLink.textContent = "Add this URL for a new widget";
			$idLink.setAttribute("href", this.api.buildUrlConfig(window.location));
			$id.append($idLink);
		}

		const $capabilities = document.createElement("matrix-widget-capabilities");
		const $capabilitiesList = document.createElement("ul");
		const $capabilitiesItems = capabilities.map((capability) => {
			const $capability = document.createElement("li");
			const $capabilityCheckbox = document.createElement("input");
			$capabilityCheckbox.setAttribute("type", "checkbox");
			$capabilityCheckbox.setAttribute("disabled", true);
			if (capabilitiesApproved?.indexOf(capability) > -1) {
				$capabilityCheckbox.setAttribute("checked", true);
			}
			const $capabilityCode = document.createElement("code");
			$capabilityCode.textContent = capability;
			$capability.append($capabilityCheckbox, $capabilityCode);
			return $capability;
		});
		$capabilitiesList.append(...$capabilitiesItems);
		$capabilities.append($capabilitiesList);

		const $info = document.createElement("matrix-widget-info");
		const $infoSummary = document.createElement("summary");
		$infoSummary.textContent = "Widget info";
		const $infoDetail = document.createElement("details");
		!capabilitiesApproved?.length && $infoDetail.setAttribute("open", true);
		$infoDetail.append($infoSummary, $id, $capabilities);
		$info.append($infoDetail);
		return $info;
	}
	createUi(uiConfig) {
		const $ui = document.createElement("matrix-widget-ui");
		const $actions = Object.entries(UI_CAPABILITIES_ACTIONS)
			.filter(([actionName]) => {
				return uiConfig?.indexOf(actionName) > -1;
			})
			.map(([actionName, actionUIConfig]) => {
				const { text, title } = actionUIConfig;
				const { value } = this.actionsState.get(actionName) || {};
				const $uiAction = document.createElement("input");
				$uiAction.addEventListener(
					"input",
					this.onCapabilityUIAction.bind(this)
				);
				$uiAction.setAttribute("type", "checkbox");
				$uiAction.setAttribute("name", actionName);
				value && $uiAction.setAttribute("checked", value);
				const $uiActionLabel = document.createElement("label");
				$uiActionLabel.setAttribute("title", title);
				$uiActionLabel.textContent = text;
				$uiActionLabel.append($uiAction);
				return $uiActionLabel;
			});
		$ui.append(...$actions);
		return $ui;
	}
	createSendCapabilities({ sendEventTypes }) {
		const $sendEvent = document.createElement("matrix-send-events");
		$sendEvent.setAttribute("profile-id", "#mock:localhost:9");
		$sendEvent.setAttribute("is-widget", true);
		$sendEvent.setAttribute("event-types", JSON.stringify(sendEventTypes));
		$sendEvent.addEventListener("mxsend", this.onMxSend.bind(this));

		const $wrapper = document.createElement("matrix-widget-send");
		const $details = document.createElement("details");
		const $summary = document.createElement("summary");
		$summary.textContent = "Send capabilities ";
		$details.append($summary, $sendEvent);
		$details.setAttribute("open", true);
		$wrapper.append($details);
		return $wrapper;
	}
	createReceiveCapabilities({ receivedEvents }) {
		const $roomContext = document.createElement("matrix-room-context");
		$roomContext.setAttribute("show-sender", true);
		$roomContext.setAttribute("show-info", true);
		/* $roomContext.setAttribute("origin", this.api.clientOrigin); */
		if (receivedEvents) {
			$roomContext.setAttribute("events", JSON.stringify(receivedEvents));
		}

		const $details = document.createElement("details");
		const $summary = document.createElement("summary");
		$summary.textContent = "Received capabilites ";
		$details.append($summary, $roomContext);
		$details.setAttribute("open", true);
		const $wrapper = document.createElement("matrix-widget-receive");
		$wrapper.append($details);
		return $wrapper;
	}
}
