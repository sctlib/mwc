import matrixApi from "../services/api.js";
import MX_DOM_EVENTS from "../services/dom-events.js";
const { ON_AUTH_STAGE, ON_AUTH_FLOW } = MX_DOM_EVENTS;

/* Example to start with a fresh template */
export default class MatrixAuthFlows extends HTMLElement {
	static get observedAttributes() {
		return ["base-url", "session", "flows", "params", "completed"];
	}
	get baseUrl() {
		return this.getAttribute("base-url");
	}
	get session() {
		return this.getAttribute("session");
	}
	get flows() {
		return JSON.parse(this.getAttribute("flows"));
	}
	get params() {
		return JSON.parse(this.getAttribute("params")) || {};
	}
	get flow() {
		return JSON.parse(this.getAttribute("flow"));
	}
	set flow(obj) {
		this.setAttribute("flow", JSON.stringify(obj));
	}
	get completed() {
		return JSON.parse(this.getAttribute("completed")) || [];
	}
	set completed(arr) {
		this.setAttribute("completed", JSON.stringify(arr));
	}
	/* state */
	completedStages = new Map();

	/* helpers */
	get flowCompleted() {
		if (!this.flow) {
			return false;
		}
		const { stages } = this.flow;
		return stages.filter((s) => !this.isStageCompleted(s)).length === 0;
	}

	/* methods */
	isStageCompleted(stage) {
		return this.completed.includes(stage);
	}

	/* events */
	onFlowSelect(event) {
		event.preventDefault();
		event.stopPropagation();
		const flowIndex = new FormData(event.target).get("flow");
		const flowIndexNum = Number(flowIndex);
		this.flow = this.flows[flowIndexNum];
		this.render();
	}
	onStageCompleted(event) {
		event.preventDefault();
		event.stopPropagation();
		const { stage, data } = event.detail;
		this.completedStages.set(stage, data);
		this.completed = [...this.completed, stage];
		if (this.flowCompleted) {
			this.dispatchEvent(
				new CustomEvent(ON_AUTH_FLOW, {
					bubbles: true,
					detail: this.completedStages,
				})
			);
		}
		this.render();
	}

	/* lifecycle */
	attributeChangedCallback() {
		this.render();
	}
	async connectedCallback() {
		this.addEventListener(ON_AUTH_STAGE, this.onStageCompleted.bind(this));
		if (this.flows?.length === 1) {
			this.flow = this.flows[0];
		}
		this.render();
	}
	disconnectedCallback() {
		this.removeEventListener(ON_AUTH_STAGE, this.onStageCompleted);
	}

	render() {
		const $doms = [];
		if (this.flowCompleted) {
			this.replaceChildren("User Interactive Authentication flow completed.");
		} else if (!this.flow && this.flows) {
			this.replaceChildren(this.createFlowSelector(this.flows));
		} else if (this.flows && this.session) {
			const $flow = this.createFlow({
				flows: this.flows,
				params: this.params,
				session: this.session,
				completed: this.completed,
				baseUrl: this.baseUrl,
			});
			this.replaceChildren($flow);
		} else {
			this.replaceChildren();
		}
	}
	createFlow({ session, flows, completed, params, baseUrl }) {
		const { stages } = this.flow;
		const $stage = stages
			.filter((stage) => !this.isStageCompleted(stage))
			.slice(0, 1)
			.map((stage) =>
				this.createStage(stage, params[stage], session, baseUrl)
			)[0];
		return $stage;
	}
	createStage(stage, params, session, baseUrl) {
		const $stage = document.createElement("matrix-auth-stage");
		$stage.setAttribute("stage", stage);
		session && $stage.setAttribute("session", session);
		baseUrl && $stage.setAttribute("base-url", baseUrl);

		const stageParams = params ? params[stage] : null;
		if (stageParams) {
			$stage.setAttribute("params", JSON.stringify(stageParams));
		}
		return $stage;
	}
	createStageCompleted(stage) {
		const $stageInput = document.createElement("input");
		$stageInput.setAttribute("type", "checkbox");
		$stageInput.setAttribute("disabled", true);
		$stageInput.checked = true;

		const $stage = this.createStage(stage);
		$stage.setAttribute("completed", true);

		const $stageLabel = document.createElement("label");
		$stageLabel.append($stageInput, $stage);

		return $stageLabel;
	}
	createFlowSelector(flows) {
		const $form = document.createElement("form");
		$form.addEventListener("submit", this.onFlowSelect.bind(this));
		const $field = document.createElement("fieldset");
		const $legend = document.createElement("legend");
		$legend.textContent = "Select user registration flow";
		const $select = document.createElement("select");
		$select.setAttribute("name", "flow");
		const $options = flows.map((flow, index) => {
			const { stages } = flow;
			const name = stages.join(" → ");
			const $option = document.createElement("option");
			$option.value = index;
			$option.textContent = name;
			return $option;
		});
		$select.append(...$options);

		const $button = document.createElement("button");
		$button.type = "submit";
		$button.textContent = "Select flow";

		$field.append($legend, $select, $button);
		$form.append($field);

		const $container = document.createElement("matrix-auth-flows-selector");
		$container.append($form);
		return $container;
	}
}
