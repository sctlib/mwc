import MatrixSendEvent from "./matrix-send-event.js";
import template from "../templates/matrix-event/m.room.message/form.js";

export default class MatrixSendMessage extends MatrixSendEvent {
	get $template() {
		return template;
	}
	get eventType() {
		return "m.room.message";
	}
}
