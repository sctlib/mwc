import api from "../services/api.js";

export default class MatrixProfile extends HTMLElement {
	static get observedAttributes() {
		return ["user-id"];
	}
	get userId() {
		return this.getAttribute("user-id") || null;
	}
	async attributeChangedCallback(attr) {
		if (attr === "user-id") {
			this.profile = await api.getUserProfile(this.userId);
		}
		this.render();
	}
	async connectedCallback() {
		if (this.userId) {
			this.profile = await api.getUserProfile(this.userId);
		}
		this.render();
	}
	render() {
		this.innerHTML = "";
		const { displayname, avatar_url } = this.profile || {};

		const $avatar = document.createElement("matrix-image");
		if (avatar_url) {
			$avatar.setAttribute("mxc", avatar_url);
		}

		const $name = document.createElement("matrix-displayname");
		if (displayname) {
			$name.innerText = displayname;
		}

		const $userId = document.createElement("matrix-userid");
		if ($userId) {
			$userId.innerText = this.userId;
		}

		this.append($userId, $avatar, $name);
	}
}
