/* the html element used in the markup (to re-use everywhere in custom elements) */
const htmlTagName = "mwc-mx-track";

/* our template, to "display" the content of an event of this type */
const template = document.createElement("template");
template.innerHTML = `
	<${htmlTagName}></${htmlTagName}>
`;

/* the web components definition, to display a "media track" as we want it */
export class LibliMatrixTrack extends HTMLElement {
	get event() {
		return JSON.parse(this.getAttribute("event"));
	}
	onPlay(event) {
		const playEvent = new CustomEvent(`${this.event.type}.play`, {
			bubbles: true,
			detail: this.event,
		});
		this.dispatchEvent(playEvent);
		// @NOTE: could imagine "registering display actions"
		// so "play" can appear in the dropdown of this event's actions
	}
	connectedCallback() {
		this.render();
	}
	render() {
		this.replaceChildren();
		const $doms = this.createDoms();
		this.append(...$doms);
	}
	createDoms() {
		const { title, url, body } = this.event?.content || {};
		const $title = document.createElement(`${htmlTagName}-title`);
		$title.innerText = title;

		const $url = document.createElement(`${htmlTagName}-url`);
		const $urlLink = document.createElement("a");
		$urlLink.setAttribute("href", url);
		$urlLink.textContent = url;
		$url.append($urlLink);

		const $body = document.createElement(`${htmlTagName}-body`);
		$body.textContent = body;

		const $actions = document.createElement(`${htmlTagName}-actions`);
		const $buttonPlay = document.createElement("button");
		$buttonPlay.addEventListener("click", this.onPlay.bind(this));
		$buttonPlay.textContent = "►";
		$actions.append($buttonPlay);

		return [$actions, $title, $url, $body];
	}
}
/* auto define the element, so not part of the main code, but can be
imported from many places */
if (!customElements.get(htmlTagName)) {
	customElements.define(htmlTagName, LibliMatrixTrack);
}

export default template;
