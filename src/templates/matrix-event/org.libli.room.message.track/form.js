const template = document.createElement("template");
template.innerHTML = `
	<form>
		<fieldset>
			<legend>Title</legend>
			<input
						name="title"
						placeholder="A small text used for title and name of this track"
			/>
		</fieldset>
		<fieldset>
			<legend>URL (youtube…)</legend>
			<input
						name="url"
						type="url"
						placeholder="The URL address to the media for this track"
			/>
		</fieldset>
		<fieldset>
			<legend>Description</legend>
			<textarea
							 name="body"
							 placeholder="A text descriptive of the media with #hashtags and @mentions"
				></textarea>
		</fieldset>
		<fieldset>
			<button type="submit">Create track</button>
		</fieldset>
	</form>
`;

export default template;
