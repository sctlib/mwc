const template = document.createElement("template");
template.innerHTML = `
	<form>
		<fieldset>
			<legend>Name</legend>
			<input
						name="name"
						placeholder="The room name"
			/>
		</fieldset>
		<fieldset>
			<button type="submit">Set room name</button>
		</fieldset>
	</form>
`;

export default template;
