/* https://spec.matrix.org/v1.9/client-server-api/#creation
	 > The homeserver will create an m.room.create event when a room is created, which serves as the root of the event graph for this room. This event also has a creator key which contains the user ID of the room creator. It will also generate several other events in order to manage permissions in this room.
	 > The client does not create this event, only displays it
 */
