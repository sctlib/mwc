const template = document.createElement("template");
template.innerHTML = `
	<form>
		<fieldset>
			<legend>Topic</legend>
			<textarea name="topic" placeholder="The room topic, description of its content"></textarea>
		</fieldset>
		<fieldset>
			<button type="submit">Set room name</button>
		</fieldset>
	</form>
`;

export default template;
