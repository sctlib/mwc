const template = document.createElement("template");
template.innerHTML = `
	<form>
		<fieldset>
			<textarea
							 name="body"
							 placeholder="Message"
							 title="A text message to send in this room"
				></textarea>
		</fieldset>
		<fieldset>
			<input
						name="url"
						type="file"
						accept="image/*, video/*"
						title="File of any matrix supported type"
				></input>
		</fieldset>
		<fieldset>
			<button type="submit" title="Send message">Send</button>
		</fieldset>
	</form>
`;

export default template;
