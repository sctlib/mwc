import "dotenv/config";

const before = async (t) => {
	/* assign an existing test user (see src/tests/.env.example file) */
	t.context.base_url = process.env.MRE_BASE_URL;
	t.context.user_id = process.env.MRE_USER_ID;
	t.context.password = process.env.MRE_USER_PASSWORD;
	t.context.room_alias = process.env.MRE_ROOM_ALIAS;
	t.context.room_id = process.env.MRE_ROOM_ID;
	t.context.space_alias = process.env.MRE_SPACE_ALIAS;
	t.context.space_id = process.env.MRE_SPACE_ID;
	t.context.room_guest_alias = process.env.MRE_ROOM_GUEST_ALIAS;
	t.context.room_guest_id = process.env.MRE_ROOM_GUEST_ID;
	t.context.room_noguest_alias = process.env.MRE_ROOM_NOGUEST_ALIAS;
	t.context.room_noguest_id = process.env.MRE_ROOM_NOGUEST_ID;
};

export { before };
