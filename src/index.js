import { defineComponents } from "@sctlib/wcu";
import api, { MatrixApi } from "./services/api.js";
import { MatrixApiWidget } from "./services/api-widget.js";
import eventsManager, {
	MatrixEventsManager,
} from "./services/events-manager.js";

import MatrixLogin from "./components/matrix-login.js";
import MatrixLogout from "./components/matrix-logout.js";
import MatrixAuth from "./components/matrix-auth.js";
import MatrixImage from "./components/matrix-image.js";
import MatrixError from "./components/matrix-error.js";
import MatrixRoom from "./components/matrix-room.js";
import MatrixRoomContext from "./components/matrix-room-context.js";
import MatrixRoomActions from "./components/matrix-room-actions.js";
import MatrixEvent from "./components/matrix-event.js";
import MatrixEventActions from "./components/matrix-event-actions.js";
import MatrixEventRoomMessage from "./components/matrix-event-room-message.js";
import MatrixProfile from "./components/matrix-profile.js";
import MatrixRoomState from "./components/matrix-room-state.js";
import MatrixRoomWidgets from "./components/matrix-room-widgets.js";
import MatrixJoinedRooms from "./components/matrix-joined-rooms.js";
import MatrixSendEvent from "./components/matrix-send-event.js";
import MatrixSendEvents from "./components/matrix-send-events.js";
import MatrixSendMessage from "./components/matrix-send-message.js";
import MatrixUser from "./components/matrix-user.js";
import MatrixJoinRoom from "./components/matrix-join-room.js";
import MatrixCreateRoom from "./components/matrix-create-room.js";
import MatrixEditRoom from "./components/matrix-edit-room.js";
import MatrixSearch from "./components/matrix-search.js";
import MatrixWidget from "./components/matrix-widget.js";
import MatrixDevices from "./components/matrix-devices.js";
import MatrixDevice from "./components/matrix-device.js";
import MatrixAuthFlows from "./components/matrix-auth-flows.js";
import MatrixAuthStage from "./components/matrix-auth-stage.js";
import MatrixRegisterEmailToken from "./components/matrix-register-email-token.js";
import MatrixRegister from "./components/matrix-register.js";
import MatrixRegisterUsernameAvailable from "./components/matrix-register-username-available.js";

const componentDefinitions = {
	"matrix-login": MatrixLogin,
	"matrix-logout": MatrixLogout,
	"matrix-register": MatrixRegister,
	"matrix-register-email-token": MatrixRegisterEmailToken,
	"matrix-register-username-available": MatrixRegisterUsernameAvailable,
	"matrix-auth": MatrixAuth,
	"matrix-auth-flows": MatrixAuthFlows,
	"matrix-auth-stage": MatrixAuthStage,
	"matrix-image": MatrixImage,
	"matrix-error": MatrixError,
	"matrix-room": MatrixRoom,
	"matrix-room-context": MatrixRoomContext,
	"matrix-room-actions": MatrixRoomActions,
	"matrix-profile": MatrixProfile,
	"matrix-event": MatrixEvent,
	"matrix-event-actions": MatrixEventActions,
	"matrix-event-room-message": MatrixEventRoomMessage,
	"matrix-room-state": MatrixRoomState,
	"matrix-room-widgets": MatrixRoomWidgets,
	"matrix-joined-rooms": MatrixJoinedRooms,
	"matrix-send-event": MatrixSendEvent,
	"matrix-send-events": MatrixSendEvents,
	"matrix-send-message": MatrixSendMessage,
	"matrix-user": MatrixUser,
	"matrix-join-room": MatrixJoinRoom,
	"matrix-create-room": MatrixCreateRoom,
	"matrix-edit-room": MatrixEditRoom,
	"matrix-search": MatrixSearch,
	"matrix-widget": MatrixWidget,
	"matrix-devices": MatrixDevices,
	"matrix-device": MatrixDevice,
};

/* Manualy define/register the web-components,
	 for ex if registering event templates */
if (window?.MWC_MANUAL_DEFINE !== true) {
	defineComponents(componentDefinitions);
}

const mwc = {
	/* singleton(s) */
	api,
	eventsManager,

	/* constructor(s) */
	MatrixApi,
	MatrixEventsManager,
	MatrixApiWidget,

	/* web-components and their utilities */
	componentDefinitions,
	defineComponents,
};

export { mwc as default };
