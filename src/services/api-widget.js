class MatrixApiWidget extends EventTarget {
	API_VERSION = "0.0.1";
	API_VERSIONS_SUPPORTED = [
		"0.0.1",
		"0.0.2",
		"org.matrix.msc2762", // send_event
		"org.matrix.msc4039", // upload_file
		"org.matrix.msc2974", // request_capabilities (not supported by element)
	];

	/*
		 names of the APIs used in the `client<->widget` communication
		 Here we're looking at things from the point of view of the widget.
		 - inbound, "from the client to the widget" (us)
		 - outbound, "from the widget to the client" (the matrix client, the room our widget is embeded into)
	 */
	DOM_EVENT_ROOT = "MATRIX_WIDGET";
	INBOUND_API_NAME = "toWidget";
	OUTBOUND_API_NAME = "fromWidget";
	CLIENT_SEARCH_PARAMS = [
		"widgetId",
		"roomId",
		"userId",
		"displayName",
		"avatarUrl",
		"parentUrl",
	];
	CAPABILITIES_ACTIONS = {
		"m.always_on_screen": ["set_always_on_screen"],
		"org.matrix.msc2762.send.event": ["send_event"],
		"org.matrix.msc2762.receive.event": ["receive_event"],
		"org.matrix.msc2974.request_capabilities": [
			"org.matrix.msc2974.request_capabilities",
		],
		"org.matrix.msc4039.upload_file": ["org.matrix.msc4039.upload_file"],
	};

	/*
		 communication between `matrix-client <-> matrix-widget`,
		 goes through the `window.postMessage` API
	 */
	outboutChannel = window?.parent;
	requestsFromWidget = new Map();
	requestsToWidget = new Map();

	/* state */
	clientOrigin = null;
	urlConfig = null;
	capabilities = null;
	capabilitiesApproved = null;
	capabilitiesActions = null;
	sendEventTypes = null;
	receiveEventTypes = null;

	/* capabilities state;
		 creates a new map from notified capabilities,
		 with getters for known types of capabilities and their actions
	 */
	setCapabilitiesApproved(approved = []) {
		this.capabilitiesApproved = approved;
		this.capabilitiesActions = approved?.reduce((acc, cap) => {
			const [capRoot, eventType] = cap.split(":");
			const capabilityActions = this.CAPABILITIES_ACTIONS[capRoot] || [];
			return [...acc, ...capabilityActions];
		}, []);
		this.sendEventTypes = approved?.reduce((acc, cap) => {
			const [action, eventType] = cap.split(":");
			if (action === "org.matrix.msc2762.send.event") {
				acc = [...acc, eventType];
			}
			return acc;
		}, []);
		this.receiveEventTypes = approved?.reduce((acc, cap) => {
			const [action, eventType] = cap.split(":");
			if (["org.matrix.msc2762.receive.event"].includes(action)) {
				acc = [...acc, eventType];
			}
			return acc;
		}, []);
	}
	hasCapability(capabilityName) {
		return this.capabilitiesApproved.indexOf(capabilityName) > -1;
	}
	getCapability(capabilityName) {
		return this.capabilitiesApproved.indexOf(capabilityName) > -1;
	}
	getActionCapability(actionName) {
		const [capability] = Object.entries(this.CAPABILITIES_ACTIONS).find(
			([capability, actions]) => {
				// will break if two capabilities have same action name
				return actions.indexOf(actionName) > -1;
			}
		);
		return capability;
	}

	/* lifecycle */
	constructor({ parent, capabilities = [] } = {}) {
		super();
		window.addEventListener("message", this.onMessage.bind(this), false);
		this.capabilities = capabilities;
		this._capabilities = capabilities;
		this._parent = parent;
		this.urlConfig = this.getUrlConfig();
		const { widgetId } = this.urlConfig;
		if (widgetId) {
			this.widgetId = widgetId;
		}
	}

	/* public methods, to call first after adding event listeners */
	async start() {
		console.log("start");
		this.sendClientAction("io.element.requires_client");
		/* this.sendClientAction("content_loaded"); */
		/* this.sendClientAction("supported_api_versions"); */

		/* (not supported?) renegotiate (for when in matrix-send-events) */
		const res = this.sendClientAction(
			"org.matrix.msc2974.request_capabilities",
			{ capabilities: this.capabilities }
		);
	}
	getUrlConfig() {
		const params = new URLSearchParams(window.location.search);
		const config = {};
		this.CLIENT_SEARCH_PARAMS.forEach((param) => {
			config[param] = params.get(param);
		});
		return config;
	}

	buildUrlConfig(origin = window.location) {
		const url = new URL(window.location);
		this.CLIENT_SEARCH_PARAMS.forEach((param) => {
			// the config URL placeholderss tart with `$`
			url.searchParams.set(param, `$${param}`);
		});
		return decodeURIComponent(url.href);
	}
	newRequestId() {
		return btoa(`@sctlib/matrix-widget-${Date.now()}`);
	}

	/* events */
	onMessage(event) {
		if (!event.origin) {
			// Handle chrome
			event.origin = event.originalEvent.origin;
		}
		// don't handle the message if not right origin / api
		if (event?.data?.api === this.INBOUND_API_NAME) {
			console.log("On client request", event.data);
			this.onClientRequest(event);
		} else if (event?.data?.api === this.OUTBOUND_API_NAME) {
			console.log("On client response", event.data);
			this.onClientResponse(event);
		}
	}
	onClientRequest(event) {
		/* then check what action comes from the client to the widget */
		const { data: clientEventData, origin: clientOrigin } = event;
		const { requestId, action, widgetId } = clientEventData;

		if (!this.clientOrigin) {
			this.clientOrigin = clientOrigin;
		}
		if (!this.widgetId && widgetId) {
			this.widgetId = widgetId;
		}

		/* keep a map of requests, and dispatch an event to users listening */
		this.requestsToWidget.set(requestId);

		this.dispatchEvent(
			new CustomEvent(`${this.DOM_EVENT_ROOT}.${action}`, {
				bubbles: true,
				detail: event.data,
			})
		);

		/*
			 let try to handle every request to our widget,
			 from the embedding-client.
			 we send a response with the requested information
		 */
		if (action === "capabilities") {
			this.sendClientResponse(clientEventData, {
				capabilities: this.capabilities,
			});
		} else if (action === "notify_capabilities") {
			const { approved, requested } = clientEventData?.data || {};
			this.setCapabilitiesApproved(approved);
			this.sendClientResponse(clientEventData, {
				capabilities: this.capabilities,
			});
			/* if (!approved && requested?.length !== this.capabilities?.length) {
				 this.sendClientAction("org.matrix.msc2974.request_capabilities", {
				 capabilities: this.capabilities,
				 });
				 } */
		} else if (action === "supported_api_versions") {
			this.sendClientResponse(clientEventData, {
				supported_versions: this.API_VERSIONS_SUPPORTED,
			});
		} else if (action === "api_version") {
			this.sendClientResponse(clientEventData, {
				version: this.API_VERSION,
			});
		} else if (action === "visibility") {
			console.info("visibility request");
		} else if (action === "screenshot") {
			this.sendClientResponse(clientEventData, {
				/* should be a blob */
				screenshot: null,
			});
		} else if (action === "send_event") {
			this.sendClientResponse(clientEventData, true);
		} else {
			console.info("Not supported incoming client message", event);
			// We'll send an error response
			this.sendClientResponse(clientEventData, {
				error: {
					message: "Action not supported",
				},
			});
		}
	}
	onClientResponse(event) {
		const { data } = event;
		const { requestId, response } = data;
		const requestResolver = this.requestsFromWidget.get(requestId);
		if (requestResolver) {
			requestResolver(response);
		} else {
			console.log("Unhandled client response", event, this.requestsFromWidget);
		}
	}

	/* send a response to the embedding-client, in the "PING/ACK" communication */
	sendClientResponse(request, response) {
		const data = { ...request, response };
		const { requestId } = request;
		this.requestsToWidget.delete(requestId);
		this.outboutChannel.postMessage(data, this.clientOrigin);
	}

	sendClientAction(action, data) {
		const requestId = this.newRequestId();
		const userRequest = {
			requestId,
			api: this.OUTBOUND_API_NAME,
			widgetId: this.widgetId,
			action,
			data,
		};
		// will be resolved by the global listener,
		return new Promise((resolve) => {
			// maintain a map to make it async
			this.requestsFromWidget.set(requestId, resolve);
			console.log("Widget sends action to client", userRequest);
			this.outboutChannel.postMessage(userRequest, this.clientOrigin);
		});
	}

	/* send an event in the room, when the widget has capability */
	sendEvent({ content, type, event_type }) {
		return this.sendClientAction("send_event", {
			type: type || event_type,
			content,
		});
	}
	sendStateEvent({ type, event_type, content, state_key }) {
		return this.sendClientAction("send_event", {
			type: type || event_type,
			state_key,
			content,
		});
	}
	uploadFile({ file, contentType, filename }) {
		return this.sendClientAction("org.matrix.msc4039.upload_file", { file });
	}
}

/* no default, because always make a new widget? (lead to debugging bug) */
/* const api = new MatrixApiWidget(); */
/* export default api; */
export { MatrixApiWidget };
