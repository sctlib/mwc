const MX_DOM_EVENTS = {
	ON_READY: "mxready",
	ON_CONTEXT: "mxcontext",
	ON_ROOM_ACTION: "mxroomaction",
	ON_ROOM_CREATE: "mxroomcreate",
	ON_EVENT_SEND: "mxsend",
	ON_EVENT_ACTION: "mxeventaction",
	ON_EVENT_SELECT: "mxeventselect",
	ON_FILE_UPLOAD: "mxfileupload",
	ON_SEARCH: "mxsearch",
	ON_REGISTER_AVAILABLE: "mxregisteravailable",
	ON_AUTH_STAGE: "mxauthstage",
	ON_AUTH_FLOW: "mxauthflow",

	/* @TODO: not well formated */
	ON_LOGIN: "user",
	ON_AUTH: "auth",

	/* globals */
	ON_WINDOW_MESSAGE: "message",
};

export { MX_DOM_EVENTS as default };
