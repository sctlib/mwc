import { getData, setData } from "../utils/local-storage.js";
import MX_DOM_EVENTS from "./dom-events.js";
const { ON_AUTH } = MX_DOM_EVENTS;

export const KEY_DB = "@sctlib/mwc";
export const KEY_USER = `user`;
export const KEY_GUEST = `user_guest`;

export default class MatrixStorageApi extends EventTarget {
	constructor(keyDb) {
		super();
		// Initialize properties in memory for synchronous access
		this.keyDb = keyDb || KEY_DB;
		this.initAuth();
	}

	// Getters for properties
	get user() {
		return this._getUser(KEY_USER);
	}
	set user(user) {
		this._setUser(KEY_USER, user);
	}
	get guestUser() {
		return this._getUser(KEY_GUEST);
	}
	set guestUser(user) {
		this._setUser(KEY_GUEST, user);
	}
	init() {
		this.initAuth();
	}
	initAuth() {
		this.user = this._getUser(KEY_USER);
		this.guestUser = this._getUser(KEY_GUEST);
	}
	_setUser(key, user) {
		if (!user) {
			setData(this.keyDb, key, null);
		} else {
			setData(this.keyDb, key, user);
		}
		this.dispatchEvent(
			new CustomEvent(ON_AUTH, {
				bubbles: true,
				detail: { user, key },
			})
		);
	}
	_getUser(key) {
		return getData(this.keyDb, key);
	}
}
