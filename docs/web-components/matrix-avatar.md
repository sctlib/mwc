# `<matrix-avatar>`

Displays an avatar media image thumbnail (for example for a room),
from a `mxc` attribute and URI.
