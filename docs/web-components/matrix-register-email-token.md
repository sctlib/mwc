# `<matrix-register-email-token>`

Will display a UI to [register an email
token](https://spec.matrix.org/v1.9/client-server-api/#post_matrixclientv3registeremailrequesttoken),
a step required in some UIA for registering a new matrix user on a
homeserver.

It will call the endpoint responsible for sending a email to the
user's email, with a validation link to prove one's email identity
with a homeserver identity services.

It is used in the matrix-auth-stage and matrix-auth-flow and the
`m.login.email.identity` stage type.
