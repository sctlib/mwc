# `<matrix-logout>`

Displays a logout button, to sign-out of the matrix user account and
guest account (both at once).
