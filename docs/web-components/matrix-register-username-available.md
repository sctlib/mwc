# `<matrix-register-username-available>`

Will check if a `username` is available to register on a `homeserver`
in order to create a new matrix user and its `user_id` (for example
`@username:homeserver.tld`).

It is currently bound to the value of `api.host` and it is therefore
not possible to register a new user on any other matrix server (by
default `matrix.org`).
