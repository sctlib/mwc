# matrix-devices

Calls the [/devices](https://spec.matrix.org/v1.9/client-server-api/#get_matrixclientv3devices) homeserver endpoint to list the current logged in user devices.

If the attribute `devices` is a JSON stringified Array of device, it
will display them, and re-render when the attribute changes.
