# web-components HTML custom elements

All elements can accept attributes that define its behavior.

> All calls to Matrix APIs need a user to be authenticated. If no user is authenticated a "guest user" is automatically created for the user.

For missing attributes and documents, check the examples, and
component definition files in `./js`. Each component should be consice
and clear enough to get an idea of what they do and which attribute
they accept.
