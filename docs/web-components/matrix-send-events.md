# `<matrix-send-events>`

Used to display a select to give the possibility for a user to send
various types of events in a same room

Set the `profile-id` and `event-types` attributes.

### `profile-id` String

A room id or alias to send events to.

### `event-types` JSON.Stringify(['m.room.message'])

The list of event types a user can send, and related forms.

> It also requires to register an event type, see appropriate API
> section.

### `is-widget` Boolean

Will be passed to the `matrix-send-event` to not submit the event to
the API, but only send the form's DOM `mxsend` event.
