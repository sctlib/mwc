# Development

1. clone repo
1. `npm install` to install the development dependenceis
1. `npm run dev` to start the local developemnt server

```
docker-compose --profile=dev up
```

## Tests

This projects uses [ava js][https://github.com/avajs/ava/] for testing library.

> Tests are currently broken, see https://gitlab.com/sctlib/mwc/-/issues/13

- run `npm run test` to run all test
- run `npx ava --match="Guest user*"` to run only matching tests ([docs](https://github.com/avajs/ava/blob/main/docs/05-command-line.md))

With docker compose:
```
docker-compose --profile=test up
```

## Development notes

The project does not use the Matrix official SDK, but a custom one available in the file `./src/services/api.js`.

## Local matrix synapse homeserver with docker
To develop against a local server with `docker` follow the docker
image
[matrixdotorg/synapse](https://hub.docker.com/r/matrixdotorg/synapse)
documentation.

> We can use `localhost` for the synapase server name

1. "Generating a configuration file"; data will be in docker volume
   `synapase-data`
1. "Running synapse"; then try accessing `localhost:8008` in a browser
1. "Generating an (admin) user"

To remove the container and the volume for this local synapase server
in docker (will remove the homeserver config and database), we can run:

```
docker stop synapse
docker volume remove synapse-data
```

To inspect the content of the docker volume, where the server
configuration is, as well as the sqlite3 database.

```
docker volume inspect synapse-data
```

To allow user registration, see
[synapase#registration](https://matrix-org.github.io/synapse/latest/usage/configuration/config_documentation.html#registration),
and edit the `homeserver.yaml` file in the docker volume, adding the
keys:

```
enable_registration: true
enable_registration_without_verification: true
```

For a full admin interface use
[synapse-admin](https://awesome-technologies.github.io/synapse-admin),
or use [element](https://app.element.io/) or
[cinny](https://app.cinny.in/) to log in the new user of the local
homeserver. Using `http://localhost:8008` as homeserver/base URL
should work in these clients.

## production

CI/CD is defined in `.gitlab.ci`

### gitlab page

The gitlab page is auto deployed when pushin on `main`, from the `pages` job

### npm package `@sctlib/mwc`

1. bump package.json version, e.g: `0.0.1`
1. https://gitlab.com/sctlib/mwc/-/tags publish a new gitlab tag (in GUI), `v0.0.1`
1. check the CI/CD pipelines for the `publish` job

### docs

- https://www.matrix.org/docs/guides/creating-a-simple-read-only-matrix-client
- https://matrix.org/docs/api
- https://matrix.org/docs/api/#get-/_matrix/media/v3/thumbnail/-serverName-/-mediaId-

# Offline first (not-working)

In firefox, to test "offline first features,
https://stackoverflow.com/questions/2647272/simulate-offline-mode-for-html5-cache-testing

Shortcut is `menu (alt) > file (f) > work offline (k)` (afk)
