# Other example usages and implementations

The mwc library is used to build other web-pages

- [matrix-site](https://gitlab.com/ugrp/tests/matrix-site) written
  mainly in HTML, registers form and display templates for
  a custom event (js)
- [libli](https://gitlab.com/sctlib/libli) as a javascript web app with a page router
