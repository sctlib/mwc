# MWC CSS styles

The matrix web components follows a style guideline with a few rules,
that should help organize why a style exist or not, and how they are
applied.

By default, this library should not apply or enforce any style, or
class/id.

A user is free to customize each components in the application they
import it to; as developer on their site, or logged in user
overwritting a configuragtion from they matrix account data (tbd).

The objectives are to keep the web-componants close to traditional
(W3C defined) HTML elements, to provide functionalities (such as with
`<p>`, `<img>`, `<details>`, `<videos>`, `<form>`, `<input>` etc.)
which can be customized by a user (visually with CSS, and in features,
by HTML attributes).

# Guidelines

- All components should work without any styles
- This library does not apply any style, CSS/HTML class or ID
- The HTML of each component should be clear in semantics and respect
  user navigation methods (keyboard defaults, mouse, touch etc.), with
  custom HTML elements, respecting accessibility and onboarding users
- Each style applies to the HTML attributes of a component, that
  represent its current state
- Use CSS variables, CSS nesting, media and component queries
- Use W3C HTML elements when meaningful
- Always provide a custom element so everything can be targetted
- Do not use shadow DOM; if necessary always export CSS parts for each
  element (event more those which have browser default styles); use
  multiple parts to group elements together (`p` or `button` etc.),
  and to make them unique (`login`, `logout` etc.)
- Don't change the name of anything comming from the matrix SPEC, so
  naming of things and semantic is respected all the ways through the
  projects (and it is simpler to reason about)

# Objectives

- Export styles that make sense, to improve UX and UI, and can be used
  as examples, template, or to have the "default look from the example
  page" (once they're good)
- Export styles as "CSS themes" (see layers too), that a user can
  chose from and edit (tbd)
- Do not add CSS/HTML class or ID (maybe offer a user to do so in the
  future, by using the components names, and a representation of the
  components and their attributes in data)
