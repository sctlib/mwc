# matrix-web-components (mwc)

All documentations for the project features are in this folder.

- all [web-components](./web-components/)
- [development](./developemnt.md)
- see the [examples](../examples/)

The project is distributed as [git
repository](https://gitlab.com/sctlib/mwc), and published as the npm
package [@sctlib/mwc](https://www.npmjs.com/package/@sctlib/mwc).

# Installation

To install for a web project, in an html file

```html
<!-- import the components -->
<script type="module" src="https://unpkg.com/@sctlib/mwc"></script>

<!-- alternatively, import and access the mwc code  -->
<script type="module">
	import mwc from "https://unpkg.com/@sctlib/mwc";
	console.log(mwc);
</script>
```

Alternatively in a javascript file, import from a CDN:

```js
import mwc from "https://unpkg.com/@sctlib/mwc";
```

or with npm:

```js
// npm install --save @sctlib/mwc
import "@sctlib/mwc";
```

# Usage

All web-components (see their docs and examples) can be used, by
inserting the custom HTML elements in any webpage after the script of
the library is loaded.

For example, to display a public matrix-room, we use the `profile-id` attribute (a matrix room ID or alias).

```html
<matrix-room profile-id="#libli-news:matrix.org"><matrix-room /></matrix-room>
```

# CSS Styles and HTML attributes API

Use the DOM elements to style the component(s) with CSS. Check the css
files in this project for some examples.

There are no HTML `class` or `id` on any web-components. To target
each element, we can use their HTML tag name and attributes, which
should reflect their internal state and the content being displayed.

> The library comes with no default styles so one can make each
> component fit the page in which they are used.
