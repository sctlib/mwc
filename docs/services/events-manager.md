# events-manager

Allows to register custom matrix events (such as `com.example.custom`
if we own the `example.com` domain and our application runs there).

It provides an interface to:

- register a `displayTemplare` to render the event when read by a user
- register a `formTemplate` to render a form to send this event type
