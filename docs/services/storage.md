# storage

used internally to communicate with the storage solution so this
application works across web-page reloads.

Curenntly using local storage, and only storing information about a
logged in user (guest, or registered user).

> In the future this interface (and the service worker attempt) should
> provide a way to "cache" the data received from the server, and sent
> by the user in a "local first and offline" approach. It could also
> be a mean to act as a "p2p web matrix server", for generating a
> matrix-static(.gitlab.io) server representation (files and folder),
> or data objects that could be sent (outside of matrix federation)
> via (sctlib.gitlab.io/)rtc mechanisms.
