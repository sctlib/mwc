# templates

The tempalte folder is currently used to organized the "events
templates" (form and display) as used by the `EventsManager` service.

They are put out of the `src/components` folder into `src/templates`
because all export a HTML `template` element, which the events manager
uses to create a new instances when displaying an event CRUD.

> This should help improve reasoning about usages of each files and
> components/functionalities they represent. By praticity we also
> organize "custom events" this way, in other projects using this
> library.

In `src/templates/matrix-events/<spec.event.name>/{form,display}.js`
are all events known to this application (tbc).

The file `matrix-events/index.js` exports each events by they type in
a javascript Object, to be able to easilly loop through them, for
example during the registration process.

## Usages

Currently templates are used only for matrix events and custom events
with the events manager.

With the form template, each "nammed input" will be used as the data
for the event, in the `.content` object; each nammed input as a named
key, with its value (currently only string).

With the display template, and `event` attribute with the content of
an event will be passed to the sole and first children of the
tempalte. Usually this template is only a single web-component
implemented by the user, which will then render a display and
functionalities for this event.

Default templates for events should be able to be overwritten by the
user implementing it in their app, by re-registering a new template (o
be checked, not sure it works).

## Ideas

- use a system similar to the events manager templates, accross
  everything that needs to be rendered as DOM in this application (not
  just `matrix-events/*` but all components; to be researched, also
  with "what render library not to use but implement?".)
- have a system to "prefill the templates" with data (for the CRUD of
  event, not yet supported)
- have a system to generate the form templates from an event
  representation as data (markdown, json)

